/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.clients;

import icr.etherj.IoUtils;
import icr.etherj.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class CliUtils
{

	/**
	 *
	 * @param dirName
	 * @return
	 */
	public static boolean ensureWritableDirectory(String dirName)
	{
		return ensureWritableDirectory(dirName, "Directory");
	}

	/**
	 *
	 * @param dirName
	 * @param description
	 * @return
	 */
	public static boolean ensureWritableDirectory(String dirName,
		String description)
	{
		if (StringUtils.isNullOrEmpty(description))
		{
			description = "Directory";
		}
		if (StringUtils.isNullOrEmpty(dirName))
		{
			System.out.println(description+" name is null or empty");
			return false;
		}
		File outDir = new File(dirName);
		if (outDir.exists())
		{
			if (!outDir.canWrite())
			{
				System.out.println(description+" not writable: "+outDir.getPath());
				return false;
			}
		}
		else
		{
			outDir.mkdirs();
		}
		return true;
	}

	/**
	 *
	 * @param fileName
	 * @return
	 */
	public static boolean isReadableDirectory(String fileName)
	{
		return isReadableDirectory(fileName, "Directory");
	}

	/**
	 *
	 * @param fileName
	 * @param description
	 * @return
	 */
	public static boolean isReadableDirectory(String fileName, String description)
	{
		if (StringUtils.isNullOrEmpty(description))
		{
			description = "Directory";
		}
		if (StringUtils.isNullOrEmpty(fileName))
		{
			System.out.println(description+" name is null or empty");
			return false;
		}
		File dir = new File(fileName);
		if (dir.exists())
		{
			if (!dir.isDirectory())
			{
				System.out.println(description+" not a directory: "+dir);
				return false;
			}
			if (!dir.canRead())
			{
				System.out.println(description+" not readable: "+dir);
				return false;
			}
		}
		else
		{
			System.out.println(description+" not found: "+dir);
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param fileName
	 * @return
	 */
	public static boolean isReadableFile(String fileName)
	{
		return isReadableFile(fileName, "File");
	}

	/**
	 *
	 * @param fileName
	 * @param description
	 * @return
	 */
	public static boolean isReadableFile(String fileName, String description)
	{
		if (StringUtils.isNullOrEmpty(description))
		{
			description = "File";
		}
		if (StringUtils.isNullOrEmpty(fileName))
		{
			System.out.println(description+" name is null or empty");
			return false;
		}
		File file = new File(fileName);
		if (file.exists())
		{
			if (!file.isFile())
			{
				System.out.println(description+" not a regular file: "+file);
				return false;
			}
			if (!file.canRead())
			{
				System.out.println(description+" not readable: "+file);
				return false;
			}
		}
		else
		{
			System.out.println(description+" not found: "+file);
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param fileName
	 * @param lines
	 * @return
	 */
	public static boolean parseFile(String fileName, Set<String> lines)
	{
		return parseFile(fileName, lines, "file");
	}

	/**
	 *
	 * @param fileName
	 * @param lines
	 * @param description
	 * @return
	 */
	public static boolean parseFile(String fileName, Set<String> lines,
		String description)
	{
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine();
			while (line != null)
			{
				line = line.trim();
				if (!line.isEmpty() && !line.startsWith("#") &&
					 !line.startsWith("//"))
				{
					lines.add(line);
				}
				line = reader.readLine();
			}
		}
		catch (IOException ex)
		{
			System.out.println("Error parsing "+description+": "+fileName);
			System.out.println(ex.getMessage());
			return false;
		}
		finally
		{
			IoUtils.safeClose(reader);
		}
		return true;
	}

	private CliUtils()
	{}
}
