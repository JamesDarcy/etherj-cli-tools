/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.clients;

import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtRoiObservationsModule;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;

/**
 *
 * @author jamesd
 */
public class RtsEdit
{
	private static final String DATETIME = "d";
	private static final String EXCLUDE = "exclude";
	private static final String EXCLUDE_FILE = "excludefile";
	private static final String HELP = "help";
	private static final String INCLUDE = "include";
	private static final String INCLUDE_FILE = "includefile";
	private static final String LABEL = "l";
	private static final String OUTPUT = "o";
	private static final String SERIES = "s";

	private String excludeFileName;
	private final Set<String> excludes = new HashSet<>();
	private final String helpMessage = "rtsedit [options] <rtsfile>";
	private final Set<String> includes = new HashSet<>();
	private String inFileName;
	private String includeFileName;
	private String label;
	private boolean noCull = false;
	private final Date now = new Date();
	private Options options;
	private String outFileName;
	private boolean resetDate;
	private int seriesNum = -1;
	private final DicomToolkit tk = DicomToolkit.getToolkit();
	private String workDir;

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		RtsEdit app = new RtsEdit();
		app.run(args);
	}

	private boolean buildExcludesFromIncludes(RtStruct rts)
	{
		List<StructureSetRoi> ssRoiList =
			rts.getStructureSetModule().getStructureSetRoiList();
		ssRoiList.forEach((x) -> excludes.add(x.getRoiName()));
		for (String include : includes)
		{
			System.out.println("Include: "+include);
			if (!excludes.contains(include))
			{
				System.out.println("ROI to include '"+include+"' not found");
				continue;
			}
			excludes.remove(include);
		}
		if (excludes.size() == ssRoiList.size())
		{
			System.out.println("Output RT-STRUCT would contain zero ROIs");
			return false;
		}
		return true;
	}

	private boolean checkFiles()
	{
		// Check input exists
		if (!CliUtils.isReadableFile(inFileName, "Input RT-STRUCT file"))
		{
			return false;
		}
		// Ensure output directory exists
		File outFile = new File(outFileName);
		if (!CliUtils.ensureWritableDirectory(outFile.getParentFile().getPath()))
		{
			return false;
		}

		// Ensure include file exists and is readable if required
		if (!StringUtils.isNullOrEmpty(includeFileName))
		{
			if (!CliUtils.isReadableFile(includeFileName, "Include file") ||
				 !CliUtils.parseFile(includeFileName, includes, "include file"))
			{
				return false;
			}
		}

		// Ensure exclude file exists and is readable if required
		if (!StringUtils.isNullOrEmpty(excludeFileName))
		{
			if (!CliUtils.isReadableFile(excludeFileName, "Exclude file") ||
				 !CliUtils.parseFile(excludeFileName, excludes, "exclude file"))
			{
				return false;
			}
		}

		return true;
	}

	private void configOptions()
	{
		options = new Options();
		options.addOption(Option.builder()
			.longOpt(HELP)
			.desc("Display this help text")
			.build());
		options.addOption(Option.builder()
			.longOpt(EXCLUDE)
			.hasArg()
			.numberOfArgs(Option.UNLIMITED_VALUES)
			.desc("List of ROI names to remove")
			.argName("roiname")
			.build());
		options.addOption(Option.builder()
			.longOpt(EXCLUDE_FILE)
			.hasArg()
			.desc("File containing a list of ROI names to remove, one ROI name per line")
			.argName("roiname")
			.build());
		options.addOption(Option.builder()
			.longOpt(INCLUDE)
			.hasArg()
			.numberOfArgs(Option.UNLIMITED_VALUES)
			.desc("List of ROI names to retain")
			.argName("roiname")
			.build());
		options.addOption(Option.builder()
			.longOpt(INCLUDE_FILE)
			.hasArg()
			.desc("File containing a list of ROI names to retain, one ROI name per line")
			.argName("roiname")
			.build());
		options.addOption(Option.builder(LABEL)
			.longOpt("label")
			.hasArg()
			.desc("Set structure set label")
			.argName("label")
			.build());
		options.addOption(Option.builder(DATETIME)
			.longOpt("datetime")
			.desc("Set structure set and series date and time to now")
			.build());
		options.addOption(Option.builder(OUTPUT)
			.longOpt("output")
			.hasArg()
			.desc("Filename for output")
			.argName("outfile")
			.build());
		options.addOption(Option.builder(SERIES)
			.longOpt("series")
			.hasArg()
			.type(Number.class)
			.desc("Set series number, implies --datetime")
			.argName("number")
			.build());
	}

	private void cullRois(RtStruct rts)
	{
		if (noCull)
		{
			return;
		}
		StructureSetModule ssm = rts.getStructureSetModule();
		RoiContourModule rcm = rts.getRoiContourModule();
		RtRoiObservationsModule rrom = rts.getRtRoiObservationsModule();
		List<Integer> roiNumbers = new ArrayList<>();
		for (StructureSetRoi ssRoi : ssm.getStructureSetRoiList())
		{
			if (excludes.contains(ssRoi.getRoiName()))
			{
				roiNumbers.add(ssRoi.getRoiNumber());
			}
		}
		for (Integer number : roiNumbers)
		{
			ssm.removeStructureSetRoi(number);
			rcm.removeRoiContour(number);
			rrom.removeRtRoiObservation(number);
		}
	}

	private boolean parseCommandLine(String[] args)
	{
		configOptions();
		CommandLineParser clParser = new DefaultParser();
		CommandLine cl;
		try
		{
			cl = clParser.parse(options, args);
		}
		catch (ParseException ex)
		{
			System.out.println(ex.getMessage());
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		// Bail on help
		if (cl.hasOption(HELP))
		{
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		// Grab working directory
		workDir = Paths.get("").toAbsolutePath().toString();
		// Grab input file name
		if (!parseInputFile(cl))
		{
			return false;
		}
		// Includes/excludes
		if (!parseIncludesExcludes(cl))
		{
			return false;
		}
		// Date reset
		resetDate = cl.hasOption(DATETIME);
		// Label 
		parseLabel(cl);
		// Replace series number+UID?
		if (!parseSeries(cl))
		{
			return false;
		}
		// Grab output file name or default if not specified.
		parseOutputFile(cl);

		return true;
	}

	private boolean parseIncludesExcludes(CommandLine cl)
	{
		// Include and exclude combined
		if ((cl.hasOption(INCLUDE) || cl.hasOption(INCLUDE_FILE)) &&
			 (cl.hasOption(EXCLUDE) || cl.hasOption(EXCLUDE_FILE)))
		{
			System.out.println(
				"Include and exclude options cannot be specified simultaneously");
			new HelpFormatter().printHelp(helpMessage, options);
			return false;			
		}
		// Neither include or exclude
		if ((!cl.hasOption(INCLUDE) && !cl.hasOption(INCLUDE_FILE)) &&
			 (!cl.hasOption(EXCLUDE) && !cl.hasOption(EXCLUDE_FILE)))
		{
			noCull = true;
			return true;
		}
		String[] includeArr = cl.getOptionValues(INCLUDE);
		if (includeArr != null)
		{
			includes.addAll(Arrays.asList(includeArr));
		}
		includeFileName = cl.getOptionValue(INCLUDE_FILE, null);
		String[] excludeArr = cl.getOptionValues(EXCLUDE);
		if (excludeArr != null)
		{
			excludes.addAll(Arrays.asList(excludeArr));
		}
		excludeFileName = cl.getOptionValue(EXCLUDE_FILE, null);

		return true;
	}

	private boolean parseInputFile(CommandLine cl)
	{
		// Arg list: remaining arguments that are not options known to the parser
		List<String> argList = cl.getArgList();
		if (argList.isEmpty())
		{
			System.out.println("Input RT-STRUCT file not found");
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		// Input file name
		inFileName = argList.get(0);
		return true;
	}

	private void parseLabel(CommandLine cl)
	{
		label = cl.getOptionValue(LABEL, null);
		if (!StringUtils.isNullOrEmpty(label))
		{
			resetDate = true;
		}
	}

	private void parseOutputFile(CommandLine cl)
	{
		outFileName = cl.getOptionValue(OUTPUT, "");
		if (outFileName.isEmpty())
		{
			File file = new File(inFileName);
			String name = "Edited_"+file.getName();
			int dotIdx = name.lastIndexOf(".");
			name = (dotIdx > 0)
				? name.substring(0, dotIdx)+".dcm"
				: name+".dcm";
			outFileName = workDir+File.separator+name;
		}
	}

	private boolean parseSeries(CommandLine cl)
	{
		try
		{
			Number number = (Number) cl.getParsedOptionValue(SERIES);
			seriesNum = (number != null) ? number.intValue() : 0;
			if (seriesNum < 0)
			{
				System.out.println("Series number must be > 0");
				new HelpFormatter().printHelp(helpMessage, options);
				return false;
			}
			resetDate = true;
		}
		catch (ParseException ex)
		{
			System.out.println("Series number must be a number > 0");
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		return true;
	}

	private boolean processIncludeExclude(RtStruct rts)
	{
		if (noCull)
		{
			return true;
		}
		// Build excludes from includes
		if (!includes.isEmpty() && !buildExcludesFromIncludes(rts))
		{
			return false;
		}
		if (excludes.isEmpty())
		{
			System.out.println("No ROIs would be removed");
			return false;
		}
		return true;
	}

	private void run(String[] args)
	{
		if (!parseCommandLine(args))
		{
			return;
		}
		if (!checkFiles())
		{
			return;
		}

		try
		{
			DicomObject dcm = DicomUtils.readDicomFile(inFileName);
			RtStruct rts = tk.createRtStruct(dcm);
			if (!processIncludeExclude(rts))
			{
				return;
			}
			// Cull based on excludes
			cullRois(rts);
			// Set label, date and series number
			setLabel(rts);
			setDate(rts);
			setSeries(rts);
			// Write the output
			saveRts(rts, outFileName);
		}
		catch (IOException ex)
		{
			System.out.println(ex.getMessage());			
		}
	}

	private void saveRts(RtStruct rts, String path) throws IOException
	{
		DicomObject dcm = new BasicDicomObject();
		Iods.pack(rts, dcm);
		DicomUtils.writeDicomFile(dcm, path);
	}

	private void setDate(RtStruct rts)
	{
		if (!resetDate)
		{
			return;
		}
		String date = DicomUtils.formatDate(now);
		String time = DicomUtils.formatTime(now, true);
		StructureSetModule ssm = rts.getStructureSetModule();
		ssm.setStructureSetDate(date);
		ssm.setStructureSetTime(time);
		RtSeriesModule rsm = rts.getRtSeriesModule();
		rsm.setSeriesDate(date);
		rsm.setSeriesTime(time);
	}

	private void setLabel(RtStruct rts)
	{
		if (StringUtils.isNullOrEmpty(label))
		{
			return;
		}
		StructureSetModule ssm = rts.getStructureSetModule();
		if (label.startsWith("+"))
		{
			ssm.setStructureSetLabel(ssm.getStructureSetLabel()+label.substring(1));
			return;
		}
		if (label.endsWith("+"))
		{
			ssm.setStructureSetLabel(label.substring(0, label.length()-1)+
				ssm.getStructureSetLabel());
			return;
		}
		ssm.setStructureSetLabel(label);
	}

	private void setSeries(RtStruct rts)
	{
		if (seriesNum < 1)
		{
			return;
		}
		RtSeriesModule rsm = rts.getRtSeriesModule();
		rsm.setSeriesNumber(seriesNum);
		rsm.setSeriesInstanceUid(Uids.generateDicomUid());
	}

}
