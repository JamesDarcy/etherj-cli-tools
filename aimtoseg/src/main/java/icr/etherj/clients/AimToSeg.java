/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.clients;

import icr.etherj.PathScan;
import icr.etherj.PathScanContext;
import icr.etherj.XmlException;
import icr.etherj.aim.AimToolkit;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.XmlParser;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.RoiConverter;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.Segmentation;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public class AimToSeg
{
	private static final String HELP = "help";
	private static final String NO3D = "no3d";
	private static final String OUTPUT = "o";
	private static final String SOURCE = "s";

	private final AimToolkit aimTk = AimToolkit.getToolkit();
	private final DicomToolkit dcmTk = DicomToolkit.getToolkit();
	private String inFileName;
	private boolean no3d = false;
	private Options options;
	private String outFileName;
	private String sourceDir;

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		AimToSeg app = new AimToSeg();
		app.run(args);
	}

	private Map<String, DicomObject> buildDicomMap(String path) throws IOException
	{
		Map<String,DicomObject> dcmMap = new HashMap<>();
		PathScan scanner = dcmTk.createPathScan();
		scanner.addContext(new DcmRx(dcmMap));
		scanner.scan(path);
		return dcmMap;
	}

	private boolean checkFiles()
	{
		// Check input exists
		if (!CliUtils.isReadableFile(inFileName, "Input AIM file"))
		{
			return false;
		}
		// Ensure output directory exists
		File outFile = new File(outFileName);
		if (!CliUtils.ensureWritableDirectory(outFile.getParentFile().getPath()))
		{
			return false;
		}
		// Ensure source directory exists and is readable
		if (!CliUtils.isReadableDirectory(sourceDir, "Source image directory"))
		{
			return false;
		}

		return true;
	}

	private void configOptions()
	{
		options = new Options();
		options.addOption(Option.builder()
			.longOpt(NO3D)
			.desc("Do not generate 3D properties such as ImagePositionPatient")
			.build());
		options.addOption(Option.builder(SOURCE)
			.longOpt("source-images")
			.hasArg()
			.desc("Directory to search for source images")
			.argName("dir")
			.build());
		options.addOption(Option.builder(OUTPUT)
			.longOpt("output")
			.hasArg()
			.desc("Filename for output")
			.argName("outfile")
			.build());
		options.addOption(Option.builder()
			.longOpt(HELP)
			.desc("Display this help text")
			.build());
	}

	private boolean parseCommandLine(String[] args)
	{
		configOptions();
		CommandLineParser clParser = new DefaultParser();
		CommandLine cl;
		String helpMessage = "aimtoseg [options] <aimfile>";
		try
		{
			cl = clParser.parse(options, args);
		}
		catch (ParseException ex)
		{
			System.out.println(ex.getMessage());
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		if (cl.hasOption(HELP))
		{
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		// Arg list: remaining arguments that are not options known to the parser
		List<String> argList = cl.getArgList();
		if (argList.isEmpty())
		{
			System.out.println("Input AIM file missing");
			new HelpFormatter().printHelp(helpMessage, options);
			return false;
		}
		// Input file name
		inFileName = argList.get(0);

		// Grab working directory
		String workDir = Paths.get("").toAbsolutePath().toString();

		// Grab output file name or default if not specified.
		outFileName = cl.getOptionValue(OUTPUT, "");
		if (outFileName.isEmpty())
		{
			File file = new File(inFileName);
			String name = "Seg_"+file.getName();
			int dotIdx = name.lastIndexOf(".");
			name = (dotIdx > 0)
				? name.substring(0, dotIdx)+".dcm"
				: name+".dcm";
			outFileName = workDir+File.separator+name;
		}

		// Grab source image dir or default if not specified
		sourceDir = cl.getOptionValue(SOURCE, "");
		if (sourceDir.isEmpty())
		{
			sourceDir = workDir;
		}

		// 2D or 3D mode
		no3d = cl.hasOption(NO3D);

		return true;
	}

	private void run(String[] args)
	{
		if (!parseCommandLine(args))
		{
			return;
		}
		if (!checkFiles())
		{
			return;
		}

		try
		{
			XmlParser parser = aimTk.createXmlParser();
			ImageAnnotationCollection iac = parser.parse(inFileName);
			Map<String,DicomObject> dcmMap = buildDicomMap(sourceDir);
			RoiConverter converter = dcmTk.createRoiConverter();
			Segmentation seg = converter.toSegmentation(iac, dcmMap, !no3d);
			IodUtils.write(seg, outFileName);
		}
		catch (IOException | XmlException | ConversionException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	private class DcmRx implements PathScanContext<DicomObject>
	{
		private final Map<String,DicomObject> dcmMap;

		public DcmRx(Map<String,DicomObject> dcmMap)
		{
			this.dcmMap = dcmMap;
		}

		@Override
		public void notifyItemFound(File file, DicomObject item)
		{
			String sopClassUid = item.getString(Tag.SOPClassUID);
			if (!DicomUtils.isImageSopClass(sopClassUid))
			{
				return;
			}
			dcmMap.put(item.getString(Tag.SOPInstanceUID), item);
		}

		@Override
		public void notifyScanFinish()
		{}

		@Override
		public void notifyScanStart()
		{}
	}

}
