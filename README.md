# EtherJ Command Line Tools

A set of simple command line tools to leverage EtherJ functionality.

## RtsToSeg

`rtstoseg` converts a set of contours in a DICOM RT-STRUCT to a set of masks in a DICOM SEG. The source images referenced in the RT-STRUCT are required to be present to retrieve attributes needed to construct the SEG. Only fully 3D modalities such as MR, CT and PET are supported. 

```text
$ rtstoseg --help
usage: rtstoseg [options] <rtsfile>
    --help                    Display this help text
 -o,--output <outfile>        Filename for output
 -s,--source-images <dir>     Directory to search for source images
 -t,--threads <threadcount>   Number of threads for parallel execution
```

## RtsEdit

`rtsedit` allows limited editing of a DICOM RT-STRUCT. An example use would be to create an RT-STRUCT with only ROIs for organs of interest from a full radiotherapy planning RT-STRUCT that includes exterior body contours and ROIs for treatment bed structures.

```text
$ rtsedit --help
usage: rtsedit [options] <rtsfile>
 -d,--datetime                Set structure set and series date and time
                              to now
    --exclude <roiname>       List of ROI names to remove
    --excludefile <roiname>   File containing a list of ROI names to
                              remove, one ROI name per line
    --help                    Display this help text
    --include <roiname>       List of ROI names to retain
    --includefile <roiname>   File containing a list of ROI names to
                              retain, one ROI name per line
 -l,--label <label>           Set structure set label
 -o,--output <outfile>        Filename for output
 -s,--series <number>         Set series number, implies --datetime
```

### Including or Excluding ROIs
ROIs specified via `--include` or `--includefile` options will be the only ROIs present in the output RT-STRUCT output file. ROIs specified via `--exclude` or `--excludefile` options will be not be present in the output RT-STRUCT file, all other ROIs from the input RT-STRUCT file will be present. Inclusion and exclusion options are mutually exclusive can cannot be used in the same invocation of `rtsedit`.

### Setting the Structure Set Label
The structure set label of the output file can be specified in three ways.

1. Replacement  
`--label SomeString` replaces the existing label with `SomeString`.
2. Prefix  
`--label SomeString+` prefixes the existing label with `SomeString` to produce `SomeStringExistingLabel`.
3. Suffix  
`--label +SomeString` adds `SomeString` as a suffix to the existing label to produce `ExistingLabelSomeString`.

Spaces after the prefix or before the suffix require the value to be quoted: `--label "SomeString +"` or `--label "+ SomeString"`.

## SegToRts

`segtorts` converts a set of contours in a DICOM SEG to a set of contours in a DICOM RT-STRUCT. The source images referenced in the SEG are required to be present to retrieve attributes needed to construct the RT-STRUCT. Only fully 3D modalities such as MR, CT and PET are supported. 

```text
$ segtorts --help
usage: segtorts [options] <segfile>
    --help                    Display this help text
 -o,--output <outfile>        Filename for output
 -s,--source-images <dir>     Directory to search for source images
```

## AimToSeg

`aimtoseg` converts a set of contours in an AIM Image Annotation Collection to a set of masks in a DICOM SEG. The source images referenced in the AIM are required to be present to retrieve attributes needed to construct the SEG. 2D modalities such as CR require the --no3d switch to be used. 

```text
$ aimtoseg --help
usage: aimtoseg [options] <aimfile>
    --help                  Display this help text
    --no3d                  Do not generate 3D properties such as ImagePositionPatient
 -o,--output <outfile>      Filename for output
 -s,--source-images <dir>   Directory to search for source images
```

